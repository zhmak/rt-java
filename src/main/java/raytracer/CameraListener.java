package raytracer;

import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;

public class CameraListener extends KeyAdapter {
	private Camera		camera;
	private RayTracer	rayTracer;

	public CameraListener(Camera camera, RayTracer rayTracer) {
		this.camera = camera;
		this.rayTracer = rayTracer;
	}

	private void render() { rayTracer.render(); }

	public void keyPressed(KeyEvent e) {
		if (e.getKeyCode() == KeyEvent.VK_UP) {
			camera.setPosition(Vec3D.add(camera.getPosition(),
				Vec3D.rotate(camera.getDirection().getBasis(), new Vec3D(0, 0, 1))));
			render();
		}
		else if (e.getKeyCode() == KeyEvent.VK_DOWN) {
			camera.setPosition(Vec3D.sub(camera.getPosition(),
				Vec3D.rotate(camera.getDirection().getBasis(), new Vec3D(0, 0, 1))));
			render();
		}
		else if (e.getKeyCode() == KeyEvent.VK_RIGHT) {
			camera.setPosition(Vec3D.add(camera.getPosition(),
				Vec3D.rotate(camera.getDirection().getBasis(), new Vec3D(1, 0, 0))));
			render();
		}
		else if (e.getKeyCode() == KeyEvent.VK_LEFT) {
			camera.setPosition(Vec3D.sub(camera.getPosition(),
				Vec3D.rotate(camera.getDirection().getBasis(), new Vec3D(1, 0, 0))));
			render();
		}
		else if (e.getKeyCode() == KeyEvent.VK_SHIFT) {
			camera.setPosition(Vec3D.add(camera.getPosition(),
				Vec3D.rotate(camera.getDirection().getBasis(), new Vec3D(0, 1, 0))));
			render();
		}
		else if (e.getKeyCode() == KeyEvent.VK_CONTROL) {
			camera.setPosition(Vec3D.sub(camera.getPosition(),
				Vec3D.rotate(camera.getDirection().getBasis(), new Vec3D(0, 1, 0))));
			render();
		}
		else if (e.getKeyCode() == KeyEvent.VK_NUMPAD2) {
			camera.getDirection().setX(camera.getDirection().getX() + RayTracer.ROTATION_STEP);
			render();
		}
		else if (e.getKeyCode() == KeyEvent.VK_NUMPAD8) {
			camera.getDirection().setX(camera.getDirection().getX() - RayTracer.ROTATION_STEP);
			render();
		}
		else if (e.getKeyCode() == KeyEvent.VK_NUMPAD4) {
			camera.getDirection().setY(camera.getDirection().getY() - RayTracer.ROTATION_STEP);
			render();
		}
		else if (e.getKeyCode() == KeyEvent.VK_NUMPAD6) {
			camera.getDirection().setY(camera.getDirection().getY() + RayTracer.ROTATION_STEP);
			render();
		}
		else if (e.getKeyCode() == KeyEvent.VK_ESCAPE) {
			rayTracer.dispose();
		}

	}
}


