package raytracer;

public class Vec3D {
	private double[] basis;

	/* CONSTRUCTORS */
	public Vec3D() { basis = new double[]{0, 0, 0}; }

	public Vec3D(double x, double y, double z) { basis = new double[]{x, y, z}; }

	public Vec3D(double[] arr) { basis = new double[]{arr[0], arr[1], arr[2]}; }

	public Vec3D(Vec3D vec) { basis = vec.getBasis(); }

	/* GETTERS */
	public double getX() { return(basis[0]); }

	public double getY() { return(basis[1]); }

	public double getZ() { return(basis[2]); }

	public double[] getBasis() { return(basis); }

	/* SETTERS */
	public void	setX(double x) { basis[0] = x; }

	public void	setY(double y) { basis[1] = y; }

	public void	setZ(double z) { basis[2] = z; }

	public void setBasis(double[] arr) {
		basis[0] = arr[0];
		basis[1] = arr[1];
		basis[2] = arr[2];
	}

	/* VECTOR STATIC METHODS */
	static public Vec3D add(Vec3D v1, Vec3D v2) {
		double[] array;
		array = new double[] {
			v1.getX() + v2.getX(),
			v1.getY() + v2.getY(),
			v1.getZ() + v2.getZ()
		};
		return (new Vec3D(array));
	}

	static public Vec3D sub(Vec3D v1, Vec3D v2) {
		double[] array;
		array = new double[] {
			v1.getX() - v2.getX(),
			v1.getY() - v2.getY(),
			v1.getZ() - v2.getZ()
		};
		return (new Vec3D(array));
	}

	static public Vec3D numMul(Vec3D v, double num) {
		double[] array;
		array = new double[] {
			v.getX() * num,
			v.getY() * num,
			v.getZ() * num
		};
		return (new Vec3D(array));
	}

	static public Vec3D numDiv(Vec3D v, double num) {
		double[] array;
		array = new double[] {
			v.getX() / num,
			v.getY() / num,
			v.getZ() / num
		};
		return (new Vec3D(array));
	}

	static public Vec3D crossProd(Vec3D vec1, Vec3D vec2) {
		double[] v1 = vec1.getBasis();
		double[] v2 = vec2.getBasis();
		double[] array = new double[]{
			v1[1] * v2[2] - v1[2] * v2[1],
			-(v1[0] * v2[2] - v1[2] * v2[0]),
			v1[0] * v2[1] - v1[1] * v2[0]
		};
		return (new Vec3D(array));
	}

	static public double dotProd(Vec3D v1, Vec3D v2) {
		return (
			v1.getX() * v2.getX()
			+ v1.getY() * v2.getY()
			+ v1.getZ() * v2.getZ()
		);
	}

	static public double len(Vec3D v) {
		double[] basis = v.getBasis();
		return (Math.sqrt(Math.pow(basis[0], 2) +
			Math.pow(basis[1], 2) + Math.pow(basis[2], 2)));
	}

	static public Vec3D normalize(Vec3D v) {
		return (Vec3D.numDiv(v, v.len()));
	}

	static public double calcCos(Vec3D v1, Vec3D v2) {
		return (Vec3D.dotProd(v1, v2) / Vec3D.len(v1) * Vec3D.len(v2));
	}

	/* VECTOR NON-STATIC METHODS */
	public void numMul(double num) {
		double[] basis = this.getBasis();
		basis[0] *= num;
		basis[1] *= num;
		basis[2] *= num;
	}

	public void numDiv(double num) {
		double[] basis = this.getBasis();
		basis[0] /= num;
		basis[1] /= num;
		basis[2] /= num;
	}

	public double len() {
		return (Vec3D.len(this));
	}

	public void normalize() { this.numDiv(this.len()); }

	public void reverseDirection() { this.numMul(-1.0); }

	/* VECTOR ROTATION STATIC METHODS */
	public static Vec3D rotateX(double angle, Vec3D v) {
		double rad = Math.toRadians(angle);
		double cos = Math.cos(rad);
		double sin = Math.sin(rad);
		return (new Vec3D(
			v.getX(),
			(v.getY() * cos - v.getZ() * sin),
			(v.getY() * sin + v.getZ() * cos))
		);
	}

	public static Vec3D rotateY(double angle, Vec3D v) {
		double rad = Math.toRadians(angle);
		double cos = Math.cos(rad);
		double sin = Math.sin(rad);
		return (new Vec3D(
			(v.getX() * cos + v.getZ() * sin),
			v.getY(),
			(-v.getX() * sin + v.getZ() * cos))
		);
	}

	public static Vec3D rotateZ(double angle, Vec3D v) {
		double rad = Math.toRadians(angle);
		double cos = Math.cos(rad);
		double sin = Math.sin(rad);
		return (new Vec3D(
			(v.getX() * cos - v.getY() * sin),
			(v.getX() * sin + v.getY() * cos),
			v.getZ())
		);
	}

	/* VECTOR ROTATION NON-STATIC METHODS */
	public void rotateX(double angle) {
		double rad = Math.toRadians(angle);
		double cos = Math.cos(rad);
		double sin = Math.sin(rad);
		double tempY = this.getY();
		this.setY(this.getY() * cos - this.getZ() * sin);
		this.setZ(tempY * sin + this.getZ() * cos);
	}

	public void rotateY(double angle) {
		double rad = Math.toRadians(angle);
		double cos = Math.cos(rad);
		double sin = Math.sin(rad);
		double tempX = this.getX();
		this.setX(this.getX() * cos + this.getZ() * sin);
		this.setZ(-tempX * sin + this.getZ() * cos);
	}

	public void rotateZ(double angle) {
		double rad = Math.toRadians(angle);
		double cos = Math.cos(rad);
		double sin = Math.sin(rad);
		double tempX = this.getX();
		this.setX(this.getX() * cos - this.getY() * sin);
		this.setY(tempX * sin + this.getY() * cos);
	}

	public static Vec3D rotate(double[] angles, Vec3D v) {
		Vec3D vec_result = new Vec3D(v.getBasis());
		if (angles[0] != 0) vec_result.rotateX(angles[0]);
		if (angles[1] != 0) vec_result.rotateY(angles[1]);
		if (angles[2] != 0) vec_result.rotateZ(angles[2]);
		return (vec_result);
	}

	public void rotate(double[] angles) {
		if (angles[0] != 0) this.rotateX(angles[0]);
		if (angles[1] != 0) this.rotateY(angles[1]);
		if (angles[2] != 0) this.rotateZ(angles[2]);
	}
}
