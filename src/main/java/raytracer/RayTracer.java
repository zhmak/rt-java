package raytracer;

import raytracer.lights.Light;
import raytracer.surfaces.Surface;

import java.util.LinkedList;

import javax.swing.*;
import java.awt.image.BufferedImage;
import java.io.File;
import javax.imageio.*;


final public class RayTracer {
	private static final int	WIN_H = 1024;
	private static final int	WIN_W = 1024;
	private static final double	FOW = 1.0;
	public static final double ROTATION_STEP = 10.0;

	private BufferedImage		image = new BufferedImage(WIN_W, WIN_H, BufferedImage.TYPE_INT_ARGB);
	private LinkedList<Surface> surfaces;
	private LinkedList<Light>	lights;
	private Camera				camera;
	private JFrame				frame;

	public RayTracer(Camera camera, LinkedList<Surface> surfaces, LinkedList<Light> lights) {
		this.camera = camera;
		this.surfaces = surfaces;
		this.lights = lights;
		this.frame = new JFrame("RT");
		frame.addKeyListener(new CameraListener(this.camera, this));
		frame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		frame.setVisible(true);
	}

	public void dispose() {
		this.frame.getContentPane().removeAll();
		this.frame.dispose();
	}

	private int checkColorMaxValue(double intensity, int color, int[] remainder) {
		remainder[0] = 0;
		if (intensity * color > 255) {
			remainder[0] = (int) ((intensity * color - 255) / 3);
			return (255);
		}
		return ((int)(intensity * color));
	}

	private Color	calcColor(Surface surface, double intensity) {
		int[] surfColors = surface.getState().getColor().getRGB();
		int[] newColors = new int[]{0, 0, 0};
		int[] remainder = new int[1];

		newColors[0] = checkColorMaxValue(intensity, surfColors[0], remainder);
		newColors[1] = surfColors[1] + remainder[0] > 255 ? 255 : surfColors[1] + remainder[0];
		newColors[2] = surfColors[2] + remainder[0] > 255 ? 255 : surfColors[2] + remainder[0];

		newColors[1] = checkColorMaxValue(intensity, newColors[1], remainder);
		newColors[0] = newColors[0] + remainder[0] > 255 ? 255 : newColors[0] + remainder[0];
		newColors[2] = newColors[2] + remainder[0] > 255 ? 255 : newColors[2] + remainder[0];

		newColors[2] = checkColorMaxValue(intensity, newColors[2], remainder);
		newColors[1] = newColors[1] + remainder[0] > 255 ? 255 : newColors[1] + remainder[0];
		newColors[0] = newColors[0] + remainder[0] > 255 ? 255 : newColors[0] + remainder[0];

		return(new Color(newColors[0], newColors[1], newColors[2]));
	}

	private Color	calcLight(Surface surface, double distance, Ray ray) {
		Vec3D	surfPoint = Vec3D.add(ray.getOrigin(), Vec3D.numMul(ray.getDirection(), distance));
		Vec3D	surfNormal = surface.calcNormal(ray, surface, surfPoint, distance);
		Vec3D	toStart = new Vec3D(ray.getDirection());
		toStart.reverseDirection();
		double	intensity = 0.0;
		for (Light currentLight : lights) {
			intensity += currentLight.calcIntensity(surfPoint, surfNormal, toStart, surface, this);
		}
		if (intensity == 0.0)
			return(new Color());
		return (calcColor(surface, intensity));
	}

	public Surface findClosestIntersection(Ray ray, double[] closestDist) {
		Surface		closestSurf = null;
		boolean		intersect;
		double		min;
		double[]	roots = new double[2];

		for (Surface currentSurf : surfaces) {
			intersect = currentSurf.intersect(ray, currentSurf, roots);
			if (intersect && roots[0] > ray.getMinLen() && roots[0] < closestDist[0]) {
				closestDist[0] = roots[0];
				closestSurf = currentSurf;
			}
			if (intersect && roots[1] > ray.getMinLen() && roots[1] < closestDist[0]) {
				closestDist[0] = roots[1];
				closestSurf = currentSurf;
			}
		}
		return (closestSurf);
	}

	private Color	traceRay(Ray ray) {
		double[]	closestDist = new double[]{Double.POSITIVE_INFINITY};
		Surface		closestSurf = findClosestIntersection(ray, closestDist);

		if (closestDist[0] >= ray.getMaxLen())
			return (new Color());
		return (calcLight(closestSurf, closestDist[0], ray));
	}

	private void drawImage() {
		/*BufferedImage test = null;
		try {
			test = ImageIO.read(new File("/Users/eloren-l/test.jpg"));
		} catch (Exception e) {
			System.out.println("exception");
		} */
		frame.getContentPane().removeAll();
		JPanel panel = new JPanel();
		JLabel label = new JLabel(new ImageIcon(image));
		panel.add(label);
		frame.add(panel);
		frame.pack();
	}

	public void render()
	{
		Ray		ray = new Ray(camera.getPosition(), 1.0, Ray.MAXIMUM_LENGTH);
		Color	color;
		int		x;
		int		y;

		y = WIN_H / 2 * -1;
		while (y < WIN_H / 2) {
			x = WIN_W / 2 * -1;
			while (x < WIN_W / 2) {
				/* need proper rotation method in vec3d to rotate rays direction vector */
				ray.calcDirection(x, -y, WIN_W, WIN_H, FOW);
				ray.getDirection().rotate(camera.getDirection().getBasis());
				color = traceRay(ray);
				image.setRGB(x + WIN_W / 2, y + WIN_H / 2, color.getIntRGB());
				x++;
			}
			y++;
		}
		drawImage();
	}
}

