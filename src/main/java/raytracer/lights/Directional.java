package raytracer.lights;

import raytracer.Ray;
import raytracer.RayTracer;
import raytracer.Vec3D;
import raytracer.surfaces.Surface;

public class Directional extends Light {
	public Directional(double luminosity) { super(luminosity); }

	public Directional(Vec3D position, double luminosity) { super(position, luminosity); }

	@Override
	public double calcIntensity(Vec3D surfPoint, Vec3D surfNormal, Vec3D toStart, Surface surface, RayTracer rayTracer) {
		Vec3D pointToLight = this.getPosition();
		Ray ray = new Ray(surfPoint, pointToLight, Ray.MINIMAL_LENGTH, Ray.MAXIMUM_LENGTH);
		double[] closestDist = new double[]{ray.getMaxLen()};
		rayTracer.findClosestIntersection(ray, closestDist);
		double intensity = 0.0;
		if (closestDist[0] > ray.getMaxLen()) {
			if (Vec3D.calcCos(surfNormal, toStart) < 0)
				surfNormal.reverseDirection();
			double cosine = Vec3D.dotProd(surfNormal, Vec3D.normalize(pointToLight));
			if (cosine > 0)
				intensity += this.getLuminosity() * cosine;
			/* code for specular here */
			// if (surface.getState().getSpecular() > 0 && cosine > 0)
			//	intensity += calcSpecular(...);
		}
		return (intensity);
	}
}
