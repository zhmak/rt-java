package raytracer.lights;

import raytracer.Ray;
import raytracer.RayTracer;
import raytracer.Vec3D;
import raytracer.surfaces.Surface;

public class Point extends Light {
	public Point(double luminosity) { super(luminosity); }

	public Point(Vec3D position, double luminosity) { super(position, luminosity); }

	/* CONSIDER MOVING CALC INTENSITY INTO LIGHT AND OVERRIDING ONLY IN AMBIENT CAUSE CODE VARIES
		ONLY FOR CALCULATION OF A RAY */
	@Override
	public double calcIntensity(Vec3D surfPoint, Vec3D surfNormal, Vec3D toStart, Surface surface, RayTracer rayTracer) {
		Vec3D pointToLight = Vec3D.sub(this.getPosition(), surfPoint);
		Ray ray = new Ray(surfPoint, pointToLight, Ray.MINIMAL_LENGTH, 1.0);
		double[] closestDist = new double[]{ray.getMaxLen()};
		double intensity = 0.0;
		if (rayTracer.findClosestIntersection(ray, closestDist) == null) {
			if (Vec3D.calcCos(surfNormal, toStart) < 0)
				surfNormal.reverseDirection();
			double cosine = Vec3D.dotProd(surfNormal, Vec3D.normalize(pointToLight));
			if (cosine > 0)
				intensity += this.getLuminosity() * cosine;
			/* code for specular here - implement calcSpecular in Light*/
			// if (surface.getState().getSpecular() > 0 && cosine > 0)
			//	intensity += calcSpecular(...);
			/* need reflectedRay for calculation of specular */
		}
		return (intensity);
	}
}
