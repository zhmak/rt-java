package raytracer.lights;

import raytracer.RayTracer;
import raytracer.Vec3D;
import raytracer.surfaces.Surface;

abstract public class Light {
	private Vec3D	position;
	private double	luminosity;

	public Light(double luminosity) {
		this.position = new Vec3D();
		this.luminosity = luminosity;
	}

	public Light(Vec3D position, double luminosity) {
		this.position = position;
		this.luminosity = luminosity;
	}

	public void setPosition(Vec3D position) { this.position = position; }

	public void setLuminosity(double luminosity) { this.luminosity = luminosity; }

	public Vec3D getPosition() { return position; }

	public double getLuminosity() { return luminosity; }

	protected double calcSpecular() {
		return (0.0);
	}

	abstract public double calcIntensity(Vec3D surfPoint, Vec3D surfNormal, Vec3D toStart, Surface surface, RayTracer rayTracer);
}
