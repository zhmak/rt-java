package raytracer.lights;

import raytracer.RayTracer;
import raytracer.Vec3D;
import raytracer.surfaces.Surface;

public class Ambient extends Light {
	public Ambient(double luminosity) { super(luminosity); }

	@Override
	public double calcIntensity(Vec3D surfPoint, Vec3D surfNormal, Vec3D toStart, Surface surface, RayTracer rayTracer) {
		return (this.getLuminosity());
	}
}
