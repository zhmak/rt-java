package raytracer.surfaces;


import raytracer.Vec3D;
import raytracer.Ray;

public class Plane extends Surface {
	public Plane(State state) { super(state); }

	@Override
	public boolean intersect(Ray ray, Surface surface, double[] roots) {
		double a = Vec3D.dotProd(surface.getState().getOrientation(), surface.getState().getPosition());
		double b = Vec3D.dotProd(surface.getState().getOrientation(), ray.getOrigin());
		double c = Vec3D.dotProd(surface.getState().getOrientation(), ray.getDirection());
		roots[0] = (a - b) / c;
		roots[1] = Double.POSITIVE_INFINITY;
		return (true);
	}

	@Override
	public Vec3D calcNormal(Ray ray, Surface surface, Vec3D surfPoint, double distance) {
		return(new Vec3D(this.getState().getOrientation().getBasis()));
	}
}
