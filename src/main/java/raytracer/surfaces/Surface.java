package raytracer.surfaces;

import raytracer.Vec3D;
import raytracer.Ray;

abstract public class Surface {
	private State state;

	public Surface() { this.state = new State(); }

	public Surface(State state) { this.state = state; }

	public State getState() { return state; }

	public void setState(State state) { this.state = state; }

	abstract public boolean intersect(Ray ray, Surface surface, double[] roots);

	abstract public Vec3D	calcNormal(Ray ray, Surface surface, Vec3D surfPoint, double distance);
}
