package raytracer.surfaces;

import raytracer.Color;
import raytracer.Vec3D;

public class State {
	private Vec3D	position;
	private Vec3D	orientation;
	private Color	color;
	private double	radius;

	public State()
	{
		this.position = new Vec3D();
		this.orientation = new Vec3D();
		this.color = new Color(255, 255, 255);
		this.radius = 1.0;
	}

	public State(Vec3D position, Vec3D orientation, Color color, double radius) {
		this.position = position;
		this.orientation = orientation;
		this.color = color;
		this.radius = radius;
	}

	public Vec3D getPosition() { return position; }

	public Vec3D getOrientation() { return orientation; }

	public Color getColor() { return color; }

	public double getRadius() { return radius; }

	public void setPosition(Vec3D position) { this.position = position; }

	public void setOrientation(Vec3D orientation) { this.orientation = orientation; }

	public void setColor(Color color) { this.color = color; }

	public void setRadius(double radius) { this.radius = radius; }
}