package raytracer.surfaces;

import raytracer.Vec3D;
import raytracer.Ray;
import raytracer.Util;

public class Cylinder extends Surface {
	public Cylinder(State state) { super(state); }

	@Override
	public boolean intersect(Ray ray, Surface surface, double[] roots) {
		return false;
	}

	@Override
	public Vec3D calcNormal(Ray ray, Surface surface, Vec3D surfPoint, double distance) {
		return null;
	}
}
