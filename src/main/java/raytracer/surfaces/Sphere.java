package raytracer.surfaces;


import raytracer.Vec3D;
import raytracer.Ray;
import raytracer.Util;

public class Sphere extends Surface {
	public Sphere(State state) { super(state); }

	@Override
	public boolean intersect(Ray ray, Surface surface, double[] roots) {
		Vec3D	centerToStart = Vec3D.sub(ray.getOrigin(), surface.getState().getPosition());
		double	a = Vec3D.dotProd(ray.getDirection(), ray.getDirection());
		double	b = 2 * Vec3D.dotProd(centerToStart, ray.getDirection());
		double	c = Vec3D.dotProd(centerToStart, centerToStart) - Math.pow(surface.getState().getRadius(), 2);
		return (Util.solveQuadratic(a, b, c, roots));
	}

	@Override
	public Vec3D calcNormal(Ray ray, Surface surface, Vec3D surfPoint, double distance) {
		return (Vec3D.sub(surfPoint, surface.getState().getPosition()));
	}
}
