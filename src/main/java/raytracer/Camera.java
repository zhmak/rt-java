package raytracer;

public class Camera {
	private Vec3D	position;
	private Vec3D   direction;

	/* CONSTRUCTORS */
	public Camera() {
		this.position = new Vec3D();
		this.direction = new Vec3D();
	}
	public Camera(Vec3D position, Vec3D direction) {
		this.position = position;
		this.direction = direction;
	}

	/* GETTERS */
	public Vec3D getPosition() { return position; }

	public Vec3D getDirection() { return (direction); }

	/* SETTERS */
	public void setPosition(Vec3D position) { this.position = position; }

	public void setDirection(Vec3D direction) { this.direction = direction; }
}
