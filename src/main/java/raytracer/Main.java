package raytracer;

import raytracer.surfaces.*;
import java.util.LinkedList;
import raytracer.lights.*;

public class Main {

	public static void main(String[] args) {
		LinkedList<Surface>	surfaces = new LinkedList<>();
		State state = new State(
			new Vec3D(0, 0, 8),
			new Vec3D(0,0,0),
			new Color(255, 0, 0),
			1);
		surfaces.add(new Sphere(state));

		state = new State(
			new Vec3D(-2, 0, 10),
			new Vec3D(0,0,0),
			new Color(0, 255, 0),
			1);
		surfaces.add(new Sphere(state));

		state = new State(
			new Vec3D(0, -2, 10),
			new Vec3D(0,0,0),
			new Color(0, 0, 255),
			1);
		surfaces.add(new Sphere(state));

		state = new State(
			new Vec3D(0, 0, 15),
			new Vec3D(0,0,1),
			new Color(0, 127, 0),
			0);
		surfaces.add(new Plane(state));

		state = new State(
			new Vec3D(0, 0, -15),
			new Vec3D(0,0,1),
			new Color(0, 255, 0),
			0);
		surfaces.add(new Plane(state));

		state = new State(
			new Vec3D(0, 15, 0),
			new Vec3D(0,1,0),
			new Color(0, 0, 127),
			0);
		surfaces.add(new Plane(state));

		state = new State(
			new Vec3D(0, -15, 0),
			new Vec3D(0,1,0),
			new Color(0, 0, 255),
			0);
		surfaces.add(new Plane(state));

		state = new State(
			new Vec3D(15, 0, 0),
			new Vec3D(1,0,0),
			new Color(127, 0, 0),
			0);
		surfaces.add(new Plane(state));

		state = new State(
			new Vec3D(-15, 0, 0),
			new Vec3D(1,0,0),
			new Color(255, 0, 0),
			0);
		surfaces.add(new Plane(state));

		LinkedList<Light>	lights = new LinkedList<>();
		lights.add(new Ambient(0.2));
		lights.add(new Point(new Vec3D(0,14, 0), 0.8));

		Camera camera = new Camera();
		RayTracer raytracer = new RayTracer(camera, surfaces, lights);

		raytracer.render();
	}
}

