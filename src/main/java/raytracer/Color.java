package raytracer;

public class Color {
	private int[]	rgb;
	private int		color;
	protected int	a;

	public Color() {
		this.rgb = new int[]{0, 0, 0};
		calcColor();
	}

	public Color(int red, int green, int blue) {
		if (red > 255 || red < 0)
			red = 255;
		if (green > 255 || green < 0)
			green = 255;
		if (blue > 255 || blue < 0)
			blue = 255;
		this.rgb = new int[]{red, green, blue};
		calcColor();
	}

	public int[] getRGB() { return rgb; }

	public int getIntRGB() { return color; }

	public void setRGB(int[] rgb) {
		this.rgb = rgb;
		calcColor();
	}

	private void calcColor() {
		int color = 0xFF000000;
		color += this.rgb[0] << 16;
		color += this.rgb[1] << 8;
		color += this.rgb[2];
		this.color = color;
	}
}
