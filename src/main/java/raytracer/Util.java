package raytracer;

final public class Util {
	private Util() { }

	public static boolean solveQuadratic(double a, double b, double c, double[] roots) {
		double discriminant = b * b - 4 * a * c;
		if (discriminant < 0) {
			roots[0] = Double.POSITIVE_INFINITY;
			roots[1] = Double.POSITIVE_INFINITY;
			return false;
		}
		roots[0] = (b * -1 + Math.sqrt(discriminant)) / (2 * a);
		roots[1] = (b * -1 - Math.sqrt(discriminant)) / (2 * a);
		return true;
	}

	public static Vec3D calcReflectedRay(){
		return null;
	}
}
