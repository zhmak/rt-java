package raytracer;

public class Ray {
	private Vec3D origin;
	private Vec3D direction;
	private double minLen;
	private double maxLen;

	final static public double MINIMAL_LENGTH = 0.01;
	final static public double MAXIMUM_LENGTH = 1000000;

	/* CONSTRUCTORS */

	public Ray(Vec3D origin) { this.origin = origin; }

	public Ray(Vec3D origin, double minLen, double maxLen) {
		this.origin = origin;
		this.direction = new Vec3D();
		this.minLen = minLen;
		this.maxLen = maxLen;
	}

	public Ray(Vec3D origin, Vec3D direction) {
		this.origin = origin;
		this.direction = direction;
		this.minLen = MINIMAL_LENGTH;
		this.maxLen = MAXIMUM_LENGTH;
	}

	public Ray(Vec3D origin, Vec3D direction, double minLen, double maxLen) {
		this.origin = origin;
		this.direction = direction;
		this.minLen = minLen;
		this.maxLen = maxLen;
	}

	/* GETTERS */
	public Vec3D getOrigin() { return origin; }

	public Vec3D getDirection() { return direction; }

	public double getMinLen() { return minLen; }

	public double getMaxLen() { return maxLen; }

	/* SETTERS */
	public void setOrigin(Vec3D origin) { this.origin = origin; }

	public void setDirection(Vec3D direction) { this.direction = direction; }

	/* METHODS */
	public void calcDirection(int x, int y, int WIN_W, int WIN_H, double FOW) {
		this.getDirection().setX((double) x / WIN_W);
		this.getDirection().setY((double) y / WIN_H);
		this.getDirection().setZ(FOW);
		this.getDirection().normalize();
	}
}

